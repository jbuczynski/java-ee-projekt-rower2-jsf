#!/bin/sh 
echo "************ UNDEPLOYING *******************"
asadmin undeploy jsfdemo
echo "************ BUILDING **********************"
cd /media/jakub/DATA/UG/java\ ee/jsfdemo
echo $PWD
mvn clean package 
echo "************ DEPLOYING *********************"
asadmin deploy /media/jakub/DATA/UG/java\ ee/jsfdemo/target/jsfdemo.war
