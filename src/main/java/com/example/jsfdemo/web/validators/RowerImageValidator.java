package com.example.jsfdemo.web.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.Part;
import java.util.ArrayList;
import java.util.List;

@FacesValidator("RowerImageValidator")
public class RowerImageValidator implements Validator{

        @Override
        public void validate(FacesContext context, UIComponent component, Object value)
                throws ValidatorException {

            List<FacesMessage> msgs = new ArrayList<FacesMessage>();
            Part file = (Part)value;
            if (file.getSize() > 1024) {
                msgs.add(new FacesMessage("file too big"));
            }
            if (!"multipart/form-data".equals(file.getContentType())) {
                msgs.add(new FacesMessage("Wrong content type, should be image."));
            }
            if (!msgs.isEmpty()) {
                throw new ValidatorException(msgs);
            }
        }
}
