package com.example.jsfdemo.web;

import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named("renderMessagesDiv")
public class renderMessagesDiv {
    public void preRenderView() {
        FacesContext.getCurrentInstance().getPartialViewContext()
                .getRenderIds().add("messages");
    }
}
