package com.example.jsfdemo.web;

import com.example.jsfdemo.domain.Rower;
import com.example.jsfdemo.service.ObjectBasedRowerStorageService;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;


@SessionScoped
@Named("rowerBean")
public class RowerFormBean implements Serializable {

        private static final long serialVersionUID = 1L;

        private Rower rower = new Rower();

        private Collection<String> availableBikeColors = Rower.Kolor.getAsStringCollection();

        private ListDataModel<Rower> rowers = new ListDataModel<Rower>();

        @Inject
        private ObjectBasedRowerStorageService ss;

        public Rower getRower() {
            return rower;
        }

        public void setRower(Rower rower) {
            this.rower = rower;
        }

        public ListDataModel<Rower> getAllRowers() {
                rowers.setWrappedData(ss.getRowery());
                return rowers;
            }

        public Collection<String> getAvailableBikeColors() {
            return availableBikeColors;
        }

        public void setAvailableBikeColors(Collection<String> availableBikeColors) {
            this.availableBikeColors = availableBikeColors;
        }

        // Actions
        public String addRower() {
            ss.addRower(rower);
            return "showBikes";
            //return null;
        }

        public String deleteRower() {
            Rower rowerToDelete = rowers.getRowData();
            ss.deleteRower(rowerToDelete);
            return null;
        }

        // Validators

        // Business logic validation
//        public void uniquePin(FacesContext context, UIComponent component,
//                              Object value) {
//
//            String pin = (String) value;
//
//            for (Person person : ss.getAllPersons()) {
//                if (person.getPin().equalsIgnoreCase(pin)) {
//                    FacesMessage message = new FacesMessage(
//                            "Person with this PIN already exists in database");
//                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
//                    throw new ValidatorException(message);
//                }
//            }
//        }
//
//        // Multi field validation with <f:event>
//        // Rule: first two digits of PIN must match last two digits of the year of
//        // birth
//        public void validatePinDob(ComponentSystemEvent event) {
//
//            UIForm form = (UIForm) event.getComponent();
//            UIInput pin = (UIInput) form.findComponent("pin");
//            UIInput dob = (UIInput) form.findComponent("dob");
//
//            if (pin.getValue() != null && dob.getValue() != null
//                    && pin.getValue().toString().length() >= 2) {
//                String twoDigitsOfPin = pin.getValue().toString().substring(0, 2);
//                Calendar cal = Calendar.getInstance();
//                cal.setTime(((Date) dob.getValue()));
//
//                String lastDigitsOfDob = ((Integer) cal.get(Calendar.YEAR))
//                        .toString().substring(2);
//
//                if (!twoDigitsOfPin.equals(lastDigitsOfDob)) {
//                    FacesContext context = FacesContext.getCurrentInstance();
//                    context.addMessage(form.getClientId(), new FacesMessage(
//                            "PIN doesn't match date of birth"));
//                    context.renderResponse();
//                }
//            }
//        }
//    }

}
