package com.example.jsfdemo.service;

import com.example.jsfdemo.domain.Rower;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ApplicationScoped
@Default
public class ObjectBasedRowerStorageService {

	private List<Rower> rowery = new ArrayList<Rower>();
	
	public ObjectBasedRowerStorageService()
	{	
		Rower r = new Rower( Rower.Kolor.czarny , "cross 1500",125125365,
				"/images/rower1.jpg", new Date() );
			
			rowery.add(r);
	}
	
	public void addRower(Rower rower)
	{
		Rower r = new Rower(rower.getKolor(), rower.getModel(), rower.getNumerSeryjny(),
			rower.getZdjeciePath(), rower.getDataProdukcji());
		
		rowery.add(r);
	}

	public void deleteRower(Rower rower) {

	}

	public List<Rower> getRowery()
	{
		return rowery;
	}
	
		
	
}
