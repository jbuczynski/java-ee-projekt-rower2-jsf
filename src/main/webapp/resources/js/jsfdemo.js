function readSingleFile(fileElementId) {
          var file = document.getElementById(fileElementId).files[0];
           if (!file) {
              return;
            }

          var reader = new FileReader();
          reader.onloadend = function() {
              content = reader.result;
              dbRequest('replaceCssFile', content);
          };
          reader.readAsText(file);

    }